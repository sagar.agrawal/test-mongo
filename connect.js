const mongoose = require('mongoose');


const mongourit = process.env.mongourit;

const mongoDB = mongoose.connect(mongourit, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.on('connected', () => {
  console.log('MongoDB connected');
});

mongoose.connection.on('error', (err) => {
  console.log('MongoDB connection error: ' + err);
});

mongoose.connection.on('disconnected', () => {
  console.log('MongoDB connection disconnected');
});

process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    console.log('MongoDB disconnecting because of app termination');
    process.exit(0);
  });
});

process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    console.log('MongoDB disconnecting because of app termination');
    process.exit(0);
  });
});

module.exports = {
  ...require('./testcoll.js'),
  mongoDB,
};
