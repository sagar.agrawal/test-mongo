const mongoose = require('mongoose');

const testcollsceme = new mongoose.Schema({
  count: Number,
  random: String,
  data: mongoose.Schema.Types.Mixed
}, { timestamps: true });


const testcollModel = mongoose.model('testcoll', testcollsceme);

module.exports = {
  testcollModel,
};
